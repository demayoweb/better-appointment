import Vue from 'vue'
import Router from 'vue-router'
import Schedule from '../pages/Schedule.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      name: 'Schedule',
      component: Schedule
    }
  ]
})
