import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import formurlencoded from 'form-urlencoded'

axios.defaults.baseURL = '/wp-json/better-appointment/v1/'

axios.interceptors.request.use((config) => {
  config.data = formurlencoded(config.data)
  return config
})

Vue.use(VueAxios, axios)
