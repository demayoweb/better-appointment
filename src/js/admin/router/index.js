import Vue from 'vue'
import Router from 'vue-router'
import Appointments from '../pages/Appointments.vue'
import AppointmentForm from '../pages/AppointmentForm.vue'
import Settings from '../pages/Settings.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Appointments',
      component: Appointments
    },
    {
      path: '/appointment/:appointment',
      name: 'AppointmentForm',
      component: AppointmentForm
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    }
  ]
})
