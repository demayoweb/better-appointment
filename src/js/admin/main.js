import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/axios'
import menuFix from './utils/admin-menu-fix'
import 'popper.js'
import 'bootstrap'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#better-appointment',
  router,
  render: h => h(App)
})

menuFix('better-appointment')
