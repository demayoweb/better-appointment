const mix = require('laravel-mix')
const LiveReloadPlugin = require('webpack-livereload-plugin')
require('laravel-mix-eslint')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix
  .js('src/js/admin/main.js', 'assets/js/admin.js')
    .eslint({
        fix: true,
        cache: false
      })
  .js('src/js/frontend/main.js', 'assets/js/frontend.js')
    .eslint({
      fix: true,
      cache: false
    })
  .sass('src/scss/admin/main.scss', 'assets/css/admin.css')
  .sass('src/scss/frontend/main.scss', 'assets/css/frontend.css')
  .webpackConfig({
    plugins: [
      new LiveReloadPlugin()
    ]
  })
  .disableNotifications()